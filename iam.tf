########################################
### IAM Policies #######################
########################################

resource "aws_iam_policy" "lambda" {
    name    = "${var.project}-${var.environment}-${var.lambda_name}"
    path    = "/"
    policy  = var.iam_json
}

########################################
### IAM Roles ##########################
########################################

resource "aws_iam_role" "lambda" {
    name = "${var.project}-${var.environment}-${var.lambda_name}"

    assume_role_policy = <<-POLICY
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "sts:AssumeRole",
                "Principal": {
                    "Service": "lambda.amazonaws.com"
                },
                "Effect": "Allow",
                "Sid": ""
            }
        ]
    }
    POLICY

    tags = {
        Environment = var.environment
        Project     = var.project
    }
}

########################################
### IAM Policy Attachments #############
########################################

resource "aws_iam_role_policy_attachment" "lambda" {
    role        = aws_iam_role.lambda.name
    policy_arn  = aws_iam_policy.lambda.arn
}

resource "aws_iam_policy_attachment" "additional_policies" {
    count       = length(var.iam_policies)
    name        = "${var.project}-${var.environment}-${var.lambda_name}-${count.index}"
    roles       = [
        aws_iam_role.lambda.name,
    ]
    policy_arn  = element(var.iam_policies, count.index)
}

