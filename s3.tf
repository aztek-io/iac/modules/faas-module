resource "aws_s3_bucket_notification" "trigger_bucket" {
    count  = length(var.buckets)
    bucket = element(var.buckets, count.index)

    lambda_function {
        lambda_function_arn = aws_lambda_function.lambda.arn
        events = [
            "s3:ObjectCreated:*"
        ]
    }
}
