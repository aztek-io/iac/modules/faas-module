resource "aws_lambda_function" "lambda" {
    s3_bucket       = var.archives_bucket
    s3_key          = "${var.lambda_name}_${var.lambda_version}.zip"
    function_name   = "${var.project}-${var.environment}-${var.lambda_name}"
    role            = aws_iam_role.lambda.arn
    handler         = var.handler
    runtime         = var.runtime
    memory_size     = var.memory
    timeout         = var.timeout
    environment {
        variables   = var.env_vars
    }

    vpc_config {
        subnet_ids          = var.vpc_config["subnet_ids"]
        security_group_ids  = var.vpc_config["security_group_ids"]
    }

    tags = {
        Environment = var.environment
        Project     = var.project
        Version     = var.lambda_version
    }
}

resource "aws_lambda_permission" "lambda" {
    count           = length(var.buckets)
    action          = "lambda:InvokeFunction"
    function_name   = aws_lambda_function.lambda.arn
    principal       = "s3.amazonaws.com"
    source_arn      = "arn:aws:s3:::${element(var.buckets, count.index)}"
}

