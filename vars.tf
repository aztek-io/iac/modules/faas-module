variable "project" {}
variable "environment" {}
variable "lambda_name" {}
variable "lambda_version" {}
variable "handler" {}

variable "runtime" {
    default = "dotnetcore2.1"
}

variable "memory" {
    default = 128
}

variable "timeout" {
    default = 15
}

variable "region" {
    default = "us-west-2"
}

variable "trigger_bucket_arn" {
    default = ""
}

variable "archives_bucket" {
    default = ""
}

variable "iam_json" {
    default = <<-IAM_JSON
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:PutLogEvents"
                ],
                "Resource": "arn:aws:logs:*:*:*"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "lambda:InvokeFunction"
                ],
                "Resource": "*"
            },
            {
                "Action": [
                    "ec2:CreateNetworkInterface",
                    "ec2:DescribeNetworkInterfaces",
                    "ec2:DeleteNetworkInterface"
                ],
                "Resource": "*",
                "Effect": "Allow"
            }
        ]
    }
    IAM_JSON

}

variable "iam_policies" {
    type = list(string)
    default = []
}

variable "env_vars" {
    type = map(string)
    default = {}
}

variable "buckets" {
    type = list(string)
    default = []
}

variable "vpc_config" {
    type    = map(list(string))
    default = {
        subnet_ids          = []
        security_group_ids  = []
    }
}
